package com.qishi.springcloud.model;

import java.io.Serializable;

/**
 * @author zhangleijie
 * @version 1.0 2019/09/26
 */

public class Person implements Serializable {

    private Integer id;

    private String name;

    private String address;

    private String context;

    private String info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
