//package com.qishi.springcloud;
//
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.context.annotation.ComponentScan;
//
///**
// * @author zhangleijie
// * @version 1.0 2019/09/26
// */
//@ComponentScan(basePackages = "com.qishi.springcloud")
//@SpringBootApplication
//public class ConfigApplication {
//
//    public static void main(String[] args) {
//        new SpringApplicationBuilder(ConfigApplication.class).properties("spring.config.location=classpath:/myapp.properties").run(args);
//    }
//}
