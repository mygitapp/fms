package com.qishi.springcloud.rule;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.Random;

/**
 * @author zhangleijie
 * @version 1.0 2019/10/08
 */
public class MyRule implements IRule {

    private ILoadBalancer loadBalancer;

    @Override
    public Server choose(Object o) {
        List<Server> serverList = loadBalancer.getAllServers();
        Random random = new Random();
        int nextValue = random.nextInt(10);
        if (nextValue > 7) {
            return chooseServerByPort(serverList, 8082);
        }
        return chooseServerByPort(serverList, 8083);
    }

    @Override
    public void setLoadBalancer(ILoadBalancer iLoadBalancer) {
        this.loadBalancer = iLoadBalancer;
    }

    @Override
    public ILoadBalancer getLoadBalancer() {
        return loadBalancer;
    }

    private Server chooseServerByPort(List<Server> servers, int port) {
        for (Server server : servers) {
            if (server.getPort() == port) {
                return server;
            }
        }
        return null;
    }
}
