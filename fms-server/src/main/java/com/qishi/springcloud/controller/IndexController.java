package com.qishi.springcloud.controller;


import com.qishi.springcloud.model.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.List;

/**
 * @author zhangleijie
 * @version 1.0 2019/09/26
 */
@RestController
public class IndexController {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/hello")
    public String hello() {
        return "hello" + Calendar.getInstance().getTimeInMillis();
    }

    @RequestMapping(value = "/getPension/{id}", method = RequestMethod.GET)
    public Person getPensionById(@PathVariable Integer id) {
        Person pension = new Person();
        pension.setId(id);
        pension.setName("zhangsan");
        pension.setAddress("beijing");
        String userName = applicationContext.getEnvironment().getProperty("test.user.name");
        pension.setContext(userName);
        return pension;
    }

    @GetMapping(value = "/count")
    @ResponseBody
    public String countService() {
        List<String> services = discoveryClient.getServices();
        for (String string : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(string);
            System.out.println("服务名称：" + string + ",服务数量：" + instances.size());
        }
        return "success";
    }

    public static Boolean isCanLinkDb = true;

    @RequestMapping(value = "/linkDb/{can}", method = RequestMethod.GET)
    public void LinkDb(@PathVariable Boolean can) {
        isCanLinkDb = can;
    }


}
