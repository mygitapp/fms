package com.qishi.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

import java.util.Scanner;

/**
 * @author zhangleijie
 * @version 1.0 2019/10/08
 */
@ComponentScan(basePackages = "com.qishi.springcloud")
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class ServerApp {

    public static void main(String[] args) {
//        @SuppressWarnings("resource")
//        Scanner scanner = new Scanner(System.in);
//        String port = scanner.nextLine();
//        new SpringApplicationBuilder(ServerApp.class).properties("server.port="+ port).run(args);
        SpringApplication.run(ServerApp.class, args);
    }

}
