package fms;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * @author zhangleijie
 * @version 1.0 2019/10/08
 */
public class SaleCommand extends HystrixCommand<String> {

    protected SaleCommand() {
        super(HystrixCommandGroupKey.Factory.asKey("myGroup"));
    }

    @Override
    protected String run() throws Exception {
        String url = "http://localhost:8080/normal";
        //1.创建一个默认的http客户端
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //2.创建一个GET请求
        HttpGet httpGet = new HttpGet(url);
        //3.获取响应
        HttpResponse httpResponse = httpClient.execute(httpGet);
        return EntityUtils.toString(httpResponse.getEntity());
    }

    @Override
    protected String getFallback() {
        return "系统异常";
    }
}
