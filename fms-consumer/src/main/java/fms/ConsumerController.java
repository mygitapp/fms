package fms;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhangleijie
 * @version 1.0 2019/09/28
 */
@RestController
public class ConsumerController {

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @GetMapping("/router")
    public String router() {
        RestTemplate restTemplate = getRestTemplate();
        return restTemplate.getForObject("http://localhost:8180/getPension/3333", String.class);
    }

}
